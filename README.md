version-test-container (based on jwilder/whoami)
======

Simple HTTP docker service that prints it's hostname and a version number fixed in the source code.

```
$ docker run -d -p 8000:8000 --name whoami -t dominikkukacka/version-test-container
5170cdde0c7b0432c2398403b79fd633b997ea2011d53714e417c7fe9e919962

$ curl 127.0.0.1:8000
I'm 5170cdde0c7b
My version is: 1.0.0
```
