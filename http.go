package main

import (
  "os"
  "fmt"
  "net/http"
  "log"
)

func main() {
    version := "CI_COMMIT_SHA"
    port := os.Getenv("PORT")
    if port == "" {
        port = "8080"
    }

    fmt.Fprintf(os.Stdout, "Listening on :%s\n", port)
    hostname, _ := os.Hostname()
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(os.Stdout, "I'm %s\n", hostname)
 	      fmt.Fprintf(w, "I'm %s\n", hostname)
        fmt.Fprintf(os.Stdout, "My Version is: %s\n", version)
        fmt.Fprintf(w, "My Version is: %s\n", version)
    })


    log.Fatal(http.ListenAndServe(":" + port, nil))
}
