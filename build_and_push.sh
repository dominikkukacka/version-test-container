#!/bin/bash

CI_COMMIT_SHA=$(git rev-parse --verify HEAD)

docker build -t "dominikkukacka/version-test-container:$CI_COMMIT_SHA" --build-arg CI_COMMIT_SHA=${CI_COMMIT_SHA} .
docker tag "dominikkukacka/version-test-container:$CI_COMMIT_SHA" "dominikkukacka/version-test-container:latest"
docker push "dominikkukacka/version-test-container:$CI_COMMIT_SHA"
docker push "dominikkukacka/version-test-container:latest"
