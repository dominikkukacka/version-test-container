#!/bin/bash

CI_COMMIT_SHA=$(git rev-parse --verify HEAD)
kubectl set image deployment/version-test-container version-test-container="dominikkukacka/version-test-container:$CI_COMMIT_SHA"

kubectl rollout status deployment/version-test-container
