FROM golang:alpine3.6 AS binary
ADD . /app
WORKDIR /app
ARG CI_COMMIT_SHA
ENV CI_COMMIT_SHA "${CI_COMMIT_SHA}"
RUN sed -i "s/CI_COMMIT_SHA/${CI_COMMIT_SHA}/g" http.go
RUN go build -o http

FROM alpine:3.6
WORKDIR /app
ENV PORT 8000
EXPOSE 8000
COPY --from=binary /app/http /app
CMD ["/app/http"]
